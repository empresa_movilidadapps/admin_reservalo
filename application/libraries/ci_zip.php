<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* // incluimos los archivos principales de la libreria
**/

require_once APPPATH . 'third_party/PHPZip/pclzip.lib.php';

class CI_Zip extends PclZip{

   protected $applied_folder;
   protected $root_folder;
   protected $filename_zip;
   protected $pattem_replace;

   public function __construct(){
      parent::__construct();

      $this->pattem_replace = array("'","\"");
   }

   public function _compressingDataZip(){

      echo "Comprimiendo informacion ...";

      return true;
   }

   public function _compressingFilesZip(){

      echo "Comprimiendo archivos ...";

      return true;
   }

   public function _compressingDirectoriesZip(){

      echo "Comprimiendo directorios ...";

      return true;
   }
}
/* End of file ci_zip.php */
/* Location: ./application/libraries/ci_zip.php */