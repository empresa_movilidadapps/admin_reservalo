<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* // incluimos los archivos principales de la libreria
**/
require_once APPPATH . 'third_party/PHPMailer/class.phpmailer.php';

class CI_Mailer extends PHPMailer{

   public function __construct(){
      parent::__construct();

      $this->IsSMTP();                                                  // establecemos que utilizaremos SMTP
      $this->SMTPAuth      = true;                                      // habilitamos la autenticación SMTP
      $this->SMTPSecure    = "ssl";                                     // establecemos el prefijo del protocolo seguro de comunicación con el servidor
      $this->Host          = "smtp.gmail.com";                          // establecemos GMail como nuestro servidor SMTP
      $this->Port          = 465;                                       // establecemos el puerto SMTP en el servidor de GMail
      $this->Username      = "minombreusuario@gmail.com";               // la cuenta de correo GMail
      $this->Password      = "contraseña";                              // password de la cuenta GMail
      // $this->SetFrom('info@tudominio.com', 'Nombre Apellido');       // Quien envía el correo
   }

   public function _sendingMail(){
      $this->AddReplyTo("response@tudominio.com","Nombre Apellido");    // A quien debe ir dirigida la respuesta
      $this->Subject       = "Asunto del correo";                       // Asunto del mensaje
      $this->Body          = "Cuerpo en HTML<br />";
      $this->AltBody       = "Cuerpo en texto plano";
      $destino             = "destinatario@otrodominio.com";
      $this->AddAddress($destino, "Juan Palotes");

      $this->AddAttachment("images/phpmailer.gif");                     // añadimos archivos adjuntos si es necesario
      $this->AddAttachment("images/phpmailer_mini.gif");                // tantos como queramos

      if(!$this->Send()){
         $data["message"] = "Error en el envío: " . $this->ErrorInfo;
      }else{
         $data["message"] = "¡Mensaje enviado correctamente!";
      }

      // $this->load->view('sent_mail',$data);
   }
}
/* End of file ci_mailer.php */
/* Location: ./application/libraries/ci_mailer.php */