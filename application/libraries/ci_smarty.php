<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* // incluimos los archivos principales de la libreria
**/

// Chama a classe do smarty
require_once APPPATH . 'third_party/PHPSmarty/Smarty.class.php';

class CI_Smarty extends Smarty{

   /**
   * Configuraçoes do Smarty
   */

   public function __construct(){
      parent::__construct();
      $this->compile_dir      = APPPATH . "views_c";
      $this->template_dir     = APPPATH . "views";
      $this->cache_dir        = APPPATH . "cache";
      $this->config_dir       = APPPATH . "config/smarty";
      $this->debugging        = FALSE;
      $this->force_compile    = FALSE;
      $this->compile_check    = TRUE;
      $this->caching          = FALSE;

      log_message('debug', "Smarty Class Initialized");
   }
}
/* End of file ci_smarty.php */
/* Location: ./application/libraries/ci_smarty.php */