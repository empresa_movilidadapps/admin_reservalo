<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* // incluimos los archivos principales de la libreria
**/
require_once APPPATH . 'third_party/PHPFPdf/fpdf.php';

class CI_Fpdf extends FPDF{

   public function __construct(){
      parent::__construct();
   }

   public function _exportingDataPdf(){
      echo "Exportando a PDF ...";
      exit();
   }
}
/* End of file ci_mailer.php */
/* Location: ./application/libraries/ci_mailer.php */