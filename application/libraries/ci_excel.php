<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* // incluimos los archivos principales de la libreria
**/
require_once APPPATH . "third_party/PHPExcel.php";

require_once APPPATH . "third_party/PHPExcel/IOFactory.php";

class CI_Excel extends PHPExcel{

   /**
   * Configuraçoes do PHPExcel
   */

   public function __construct(){
      parent::__construct();
   }

   public function _exportingDataExcel($data){
      echo "Exportando datos a excel ...";

      return TRUE;
   }
}
/* End of file ci_excel.php */
/* Location: ./application/libraries/ci_excel.php */