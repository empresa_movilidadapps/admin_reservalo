<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	private static $instance;

	protected $_zip;
	protected $_pdf;
	protected $_view;
	protected $_mail;
	protected $_excel;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		self::$instance =& $this;

		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}

		$this->load =& load_class('Loader', 'core');

		$this->load->initialize();
		
		log_message('debug', "Controller Class Initialized");

		/*** // Configuracion MovilidadApps **/

		$this->_zip 	= $this->ci_zip;
		$this->_pdf 	= $this->ci_fpdf;
		$this->_view 	= $this->ci_smarty;
		$this->_mail 	= $this->ci_mailer;
		$this->_excel 	= $this->ci_excel;

		$this->_view->assign('public', APPLICATION . MOVAPPS_FOLDER_PUBLIC);
		$this->_view->assign('resources', APPLICATION . MOVAPPS_FOLDER_RESOURCES);
		$this->_view->assign('tmp', APPLICATION . MOVAPPS_FOLDER_TMP);

		// if(isset($this->session->userdata['id_usuario'])){
		// 	$this->_view->assign("usuario_session",$this->session->userdata);
		// }

		$this->_loadGlobalPaths();
	}

	public static function &get_instance()
	{
		return self::$instance;
	}

	protected function _loadGlobalPaths(){
		try{
			/*** Definimos las rutas que usaremos en la aplicacion */
			$path_admin_verificar_login = site_url("administrador/verificar_login");
			/*** Asiganamos las rutas a las vista que mostremos */
			$this->_view->assign("path_admin_verificar_login",$path_admin_verificar_login);

			return TRUE;
		}catch (Exception $e){
			return FALSE;
		}
	}
}
// END Controller class

/* End of file Controller.php */
/* Location: ./system/core/Controller.php */