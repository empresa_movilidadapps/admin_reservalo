<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Constructor
	 *
	 * @access public
	 */
	function __construct()
	{
		log_message('debug', "Model Class Initialized");
	}

	/**
	 * __get
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	/*** // Configuracion MovilidadApps **/

	/*** // Simples Queries */

	protected function _executeSimpleQuery($query){
		try{
			$result = $this->db->query($query);

			if($result)
				return $this->_toArrays($result);

			return FALSE;
		}catch(Exception $e){
			return FALSE;
		}
	}

	protected function _executeSimpleInsert($table, $data){
		try{
			$result = $this->db->insert($table, $data);

			if($result)
				return $this->_toArrays($result);

			return FALSE;
		}catch (Exception $e){
			return FALSE;
		}
	}

	protected function _executeSimpleUpdate($table, $keys, $data){
		try{
			$result = $this->db->update($table, $data, $keys);

			if($result)
				return $this->_toArrays($result);

			return FALSE;
		}catch (Exception $e){
			return FALSE;
		}
	}

	protected function _executeSimpleDelete($table, $keys){
		try{
			$result = $this->db->update($table, $keys);

			if($result)
				return $this->_toArrays($result);

			return FALSE;
		}catch (Exception $e){
			return FALSE;
		}
	}

	/*** // Advanced Queries */

	// protected function _executeAdvancedQuery($query){}
	// protected function _executeAdvancedInsert($table, $data){}
	// protected function _executeAdvancedUpdate($table, $keys, $data){}
	// protected function _executeAdvancedDelete($table, $keys){}

	/*** // Extra Queries */

	protected function _executeUpdateDelete($table, $keys){}
	protected function _executeRestoreRecord($table, $keys){}

	/*** // Convertir en un arreglo, el objeto devuelto por la consulta PDO. */

	protected function _toArrays($data){
		try{
			$data_array = array();

			if(is_array($data)){
				foreach($data as $d){
					$data_array[] = $d->result_array();
				}
			}else if(is_object($data)){
				if($data->num_rows() == 1)
					return $data->row_array();

				return $data->result_array();
			}

			return $data_array;
		}catch(Exception $e){
			return FALSE;
		}
	}
}
// END Model Class

/* End of file Model.php */
/* Location: ./system/core/Model.php */